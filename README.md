# surya-practical-test

## Deployment flow
![deployment flow](img/flow.png)

## Step by step until failure :

Accessing the cluster using command 

```
# export KUBECONFIG=~/.kube/kubeconfig-surya
```
And then examine the cluster using
```
# kubectl cluster-info (to show cluster details)
# kubectl get nodes (to show nodes available)
```

To install gitlab on cluster, I need to add repos and update the charts
```
# helm repo add gitlab https://charts.gitlab.io/ 
# helm repo update
```
Next, add namespace to contain all pods for deployment
```
# kubectl create namespace gitlab
```

After creating namespace, I can deploy it inside the namespace using this command

```
# helm install gitlab gitlab/gitlab   \
--timeout 600s \
--set global.hosts.domain=surya.technical-test.catapa.com   \
--set certmanager-issuer.email=rearking05@gmail.com   \
--set global.kas.enabled=true   \
--set certmanager.install=false
```
Those command above are for installing the gitlab inside it’s namespaces and using global host domain as “hostname” that can be accessed from Nginx LoadBalancer, but there’s my problem, I don’t know the externalIP parameter that I should add, for testing I’ve tried many IP (e.g 108.136.144.105 and 34.143.242.120) I use the VPN because I think I should use it to “make the services only can be accessed using that ip” but when I tried to access the ingress via domain name e.g. gitlab.surya.practical-test.catapa.com it won’t open. And when I tried to access using the LB (ip that got from cluster allocation of external IP) it return 404 not found from NGINX. After hours of searching and finding the root of cause (I’m inspecting the pods using kubectl describe <pod name> -n gitlab) there’s no error but it not works, I also inspecting using kubectl get events -n gitlab it’s about the same. And I’m also tried to check the logs using kubectl logs <pod name> -n gitlab, it’s says the pods are working but not serving.

Here’s my latest attachment for the report (I forgot to change the context into namespace gitlab, so the resources are inside “default” namespace)
![get ingress](img/get_ingress.png)

Here’s the service when I tried to check them
![get svc](img/get_svc.png)


## Closure
I’m sorry for the failure that I’ve done during practical test, that’s purely from my incompetence and lacking of experience using container orchestration (this case are Kubernetes) because my previous experience are just developing website, packing it on container, and ship that into knative and cloud run for deployment. This is my new experience of hands on into cluster and deploying an apps and release it inside cluster. I found this more challenging and makes me think that I should learning more in-depth on deployment and building infrastructure using Kubernetes. Thank you for the opportunities that you guys given to me, I hope we can work together in future. 
